#include "Particle.h"
#include <iostream>

using namespace std;

Particle::Particle ( float mass, const Vec3& position, float radius,float damping )
    : _damping ( damping ), _mass ( mass ),    _position ( position ) , _velocity ( 0,0,0 ),
      _forceAcc ( 0,0,0 ), _restitution ( 0.8f ), _gravity ( 0,-10,0 ), _radius ( radius )
{
    assert ( mass > 0.0f );
}

Particle::~Particle()
{
    std::cout << "Particle::delete particle ok !" << std::endl;
}

void Particle::integrate ( float dt )
{
    Vec3 acceleration = _gravity + ( _forceAcc / _mass );
    const Vec3 deltaVelocity = acceleration * dt;
    _velocity += deltaVelocity;
    _velocity *=  _damping;
    const Vec3 deltaPosition = _velocity * dt;
    _position += deltaPosition;
    _forceAcc = Vec3 ( 0,0,0 );
    
    _data.aabb = getAABB(); //RECALCULA AABB
}

void Particle::addForce ( const Vec3& force )
{
    _forceAcc += force;
}


Vec3 Particle::getPosition() const
{
    return _position;
}






























