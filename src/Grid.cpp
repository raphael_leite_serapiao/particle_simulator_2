#include "Grid.h"
#include <iostream>

using namespace std;


Grid::Grid ( const Aabb& gridAABB, unsigned xNumberOfCells, unsigned yNumberOfCells, unsigned zNumberOfCells )
    : _gridAABB ( gridAABB )
{
    assert ( xNumberOfCells > 0u && "err1" );
    assert ( yNumberOfCells > 0u && "err2" );
    assert ( zNumberOfCells > 0u && "err3" );

    _numberOfCells[0] = xNumberOfCells;
    _numberOfCells[1] = yNumberOfCells;
    _numberOfCells[2] = zNumberOfCells;

    _cellDimensions[0] = gridAABB.getWidth() / float ( xNumberOfCells );
    _cellDimensions[1] = gridAABB.getHeight() / float ( yNumberOfCells );
    _cellDimensions[2] = gridAABB.getDepth() / float ( zNumberOfCells );

    _cells = std::vector<std::vector<std::vector<Cell> > > (
                 xNumberOfCells, std::vector<std::vector<Cell> > ( yNumberOfCells, std::vector<Cell> ( zNumberOfCells ) )
             );
}






Grid::Grid ( const Vec3& gridCenter, const Vec3& cellDimensions, unsigned xNumberOfCells, unsigned yNumberOfCells, unsigned zNumberOfCells )
{
    assert ( xNumberOfCells > 0u );
    assert ( yNumberOfCells > 0u );
    assert ( zNumberOfCells > 0u );

    _numberOfCells[0] = xNumberOfCells;
    _numberOfCells[1] = yNumberOfCells;
    _numberOfCells[2] = zNumberOfCells;

    _cellDimensions[0] = cellDimensions[0];
    _cellDimensions[1] = cellDimensions[1];
    _cellDimensions[2] = cellDimensions[2];

    const Vec3 gridDimensions (
        cellDimensions._x * xNumberOfCells,
        cellDimensions._y * yNumberOfCells,
        cellDimensions._z * zNumberOfCells
    );

    const Vec3 minPoint = gridCenter - gridDimensions/2.0f;
    const Vec3 maxPoint = gridCenter + gridDimensions/2.0f;

    _gridAABB = Aabb ( minPoint, maxPoint );

    _cells = std::vector<std::vector<std::vector<Cell> > > (
                 xNumberOfCells, std::vector<std::vector<Cell> > ( yNumberOfCells, std::vector<Cell> ( zNumberOfCells ) )
             );
}






bool Grid::getIndexOfPoint ( const Vec3& point, CellIndex& index ) const
{
    const Vec3 pointLocal = point - _gridAABB.getMin();

    for ( unsigned i = 0u; i < 3u; ++i )
    {

        if ( pointLocal[i] < 0.0f )
        {
            return false;
        }

        index[i] = unsigned ( pointLocal[i] / _cellDimensions[i] );

        if ( index[i] >= _numberOfCells[i] )
        {
            return false;
        }

    }

    cout << "point:           :" << point << endl;
    cout << "_gridAABB.getMin : "<< _gridAABB.getMin() << endl;
    cout << "pointLocal       :" << pointLocal << endl;
    cout << endl;

    return true;
}






Aabb Grid::getCellAABB ( const CellIndex& index ) const
{
    Vec3 minPoint, maxPoint;

    for ( unsigned i = 0u; i < 3u; ++i )
    {
        minPoint[i] = _gridAABB.getMin() [i] + index[i] * _cellDimensions[i];
        maxPoint[i] = minPoint[i] + _cellDimensions[i];
    }

    return Aabb ( minPoint, maxPoint );
}






void Grid::_readCells ( Data* data ) const
{
    CellIndex minIndex, maxIndex;

    if ( !getIndexOfPoint ( data->aabb.getMin(), minIndex ) )
    {
        return; // min point is outside this grid
    }

    if ( !getIndexOfPoint ( data->aabb.getMax(), maxIndex ) )
    {
        return; // max point is outside this grid
    }

    for ( unsigned i = minIndex[0]; i <= maxIndex[0]; ++i )
    {
        for ( unsigned j = minIndex[1]; j <= maxIndex[1]; ++j )
        {
            for ( unsigned k = minIndex[2]; k <= maxIndex[2]; ++k )
            {
                data->cells.push_back ( CellIndex ( i,j,k ) );
            }
        }
    }
}






void Grid::update ( Data* data )
{
//     remove from current cells
//     percorre todo o grid e remove as ocorrencias da particula
    for ( auto it = data->cells.begin(); it != data->cells.end(); ++it )
    {
        _cells[it->i][it->j][it->k].erase ( data );
    }

//     calculate new cells
//     limpa da particulas as coordenadas do grid
    data->cells.clear();


    _readCells ( data ); //prencher de novo os cells e calcula nova posicao

    // insert in new cells
    for ( auto it = data->cells.begin(); it != data->cells.end(); ++it )
    {
        _cells[it->i][it->j][it->k].insert ( data );
    }
}






std::set<Grid::Data*> Grid::getCloseData ( const Data* data ) const
{
    std::set<Data*> closeData;

    for ( auto it = data->cells.begin(); it != data->cells.end(); ++it )
    {
        const Cell& cell = _cells[it->i][it->j][it->k];

        for ( auto it2 = cell.begin(); it2 != cell.end(); ++it2 )
        {
            if ( data->aabb.intersects ( ( *it2 )->aabb ) )
            {
                closeData.insert ( *it2 );
            }
        }
    }

    closeData.erase ( ( Data* ) data );
    return closeData;
}





















