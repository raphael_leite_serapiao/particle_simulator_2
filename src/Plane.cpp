#include "Plane.h"
#include <iostream>

Plane::Plane ( const Vec3& normal, float distance ) : _n ( normal ), _d ( distance )
{
}

Plane::~Plane()
{
    std::cout << "Plane::delete plane ok ..." << std::endl;
}


float Plane::distance ( const Vec3& point ) const
{
    return _n.dot ( point ) - _d;
}



const Vec3& Plane::getNormal() const
{
    return _n;
}
