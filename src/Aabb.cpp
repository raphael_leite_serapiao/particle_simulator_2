#include "Aabb.h"

#include <iostream>

Vec3 Aabb::getVertex ( unsigned int i ) const
{
    assert ( i <= 8u && "Value max 8" );


    const static Vec3 matrix[8] =
    {
        Vec3 ( -1,-1,-1 ),
        Vec3 ( -1,-1, 1 ),
        Vec3 ( -1, 1,-1 ),
        Vec3 ( -1, 1, 1 ),
        Vec3 ( 1,-1,-1 ),
        Vec3 ( 1,-1, 1 ),
        Vec3 ( 1, 1,-1 ),
        Vec3 ( 1, 1, 1 )
    };

    const Vec3 middleBounds ( getWidth() /2, getHeight() /2, getDepth() /2 );
    const Vec3 center = getCenter();

    const Vec3 result =  center + middleBounds * matrix[i];
//     std::cout << middleBounds << " + " << center << " * " << matrix[i] << " = " << result << std::endl;
    return result;
}


std::array< Vec3, 8u > Aabb::getVertices() const
{
    std::array<Vec3,8> vertices;
    for ( unsigned i = 0; i < 8; i++ )
        vertices[i] = getVertex ( i );
    return vertices;
}



std::array< std::pair< Vec3, Vec3 >, 12u > Aabb::getEdges() const
{
    std::array<Vec3,8> vertices = getVertices();
    std::array<std::pair<Vec3,Vec3>, 12u> edges;

    edges[0] =  std::make_pair ( vertices[0],vertices[1] );
    edges[1] =  std::make_pair ( vertices[2],vertices[3] );
    edges[2] =  std::make_pair ( vertices[6],vertices[7] );
    edges[3] =  std::make_pair ( vertices[4],vertices[5] );
    edges[4] =  std::make_pair ( vertices[1],vertices[5] );
    edges[5] =  std::make_pair ( vertices[3],vertices[7] );
    edges[6] =  std::make_pair ( vertices[2],vertices[6] );
    edges[7] =  std::make_pair ( vertices[0],vertices[4] );
    edges[8] =  std::make_pair ( vertices[0],vertices[2] );
    edges[9] =  std::make_pair ( vertices[1],vertices[3] );
    edges[10] = std::make_pair ( vertices[5],vertices[7] );
    edges[11] = std::make_pair ( vertices[4],vertices[6] );

    return edges;
}



bool Aabb::intersects ( const Aabb& other ) const
{
    return ( _minPoint._x <= other._maxPoint._x ) &&
           ( _maxPoint._x >= other._minPoint._x ) &&
           ( _minPoint._y <= other._maxPoint._y ) &&
           ( _maxPoint._y >= other._minPoint._y ) &&
           ( _minPoint._z <= other._maxPoint._z ) &&
           ( _maxPoint._z >= other._minPoint._z );
}

