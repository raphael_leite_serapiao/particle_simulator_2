#include "Vec3.h"

std::ostream& operator<< ( std::ostream& out, const Vec3& v )
{
    out << "(" << v._x << ", " << v._y << ", " << v._z << ")";
    return out;
}