#include <GL/glut.h>
#include <GL/freeglut.h>
#include <iostream>

#include "ParticleManager.h"
#include "Aabb.h"
#include "Grid.h"

using namespace std;


int _prevTime = 0;
int mouseX, mouseY;
Vec3 _cameraPosition ( 0,20, 0 );



// Aabb _a1 ( Vec3 ( 2,2,2 ), Vec3 ( 8,8,8 ) );
// Grid _grid ( Vec3 ( 0,0,0 ), Vec3 ( 1,1,1 ), 8u, 8u, 8u );
// Grid::CellIndex cell;
// float pitch = 0, yaw = 0, valor = 60;


void draw ( const Grid& grid );
void draw ( const Aabb& aabb );


float getDeltaTime()
{
    const int timeNow = glutGet ( GLUT_ELAPSED_TIME );
    const float dt = ( timeNow - _prevTime ) / 1000.0f;
    _prevTime = timeNow;
    return dt;
}





void update ()
{
    ParticleManager::getInstance()->update ( getDeltaTime(), 0.02f, 10 );
//     for ( unsigned i = 0; i < ParticleManager::getInstance()->getNumberParticles(); i++ )
//     {
//         ParticlePtr p = ParticleManager::getInstance()->getParticle ( i );
//         _grid.update ( p->getData() );
//     }
    glutPostRedisplay();
}





void drawParticle ( const ParticlePtr p )
{
    glPushMatrix();
    glTranslatef ( p->getPosition()._x, p->getPosition()._y, p->getPosition()._z );
    glColor3f ( 1,0,0 );
    glutSolidSphere ( p->getRadius(),10,10 );
    glPopMatrix();
}





void draw ( const Grid& grid )
{
    //cout << "draw" << endl;
    for ( unsigned i = 0u; i < grid.getNumberOfCells ( 0 ); ++i )
    {
        for ( unsigned j = 0u; j < grid.getNumberOfCells ( 1 ); ++j )
        {
            for ( unsigned k = 0u; k < grid.getNumberOfCells ( 2 ); ++k )
            {
                const Grid::CellIndex cellIndex ( i,j,k );
// 		if ( grid.getCellData ( cellIndex ).size() )
//                 {
                draw ( grid.getCellAABB ( cellIndex ) );
//                 }
            }
        }
    }
}





void draw ( const Aabb& aabb )
{
    const std::array<std::pair<Vec3, Vec3>, 12u> edges = aabb.getEdges();
    glBegin ( GL_LINES );

    for ( unsigned i = 0u; i < 12u; ++i )
    {
        glVertex3f ( edges[i].first._x, edges[i].first._y, edges[i].first._z );
        glVertex3f ( edges[i].second._x, edges[i].second._y, edges[i].second._z );
    }
    glEnd();
}



void drawLines()
{
    ParticlePtr p = ParticleManager::getInstance()->getParticle ( 0 );
    glColor3f ( 1,1,0 );
    glBegin ( GL_LINES );
    glVertex3f ( p->getPosition()._x, p->getPosition()._y,  p->getPosition()._z );
//     glVertex3f ( yaw, 0, -pitch );
//     5cout << p->getPosition() << endl;
    glEnd();
}


void draw()
{
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();

    glPushMatrix();

    //glRotatef ( -pitch, 1,0,0 );
    //glRotatef ( -yaw, 0,1,0 );

    glRotatef ( 90, 1, 0, 0 );
    glTranslatef ( -_cameraPosition._x, -_cameraPosition._y, -_cameraPosition._z );

    for ( unsigned i = 0; i < ParticleManager::getInstance()->getNumberParticles(); i++ )
    {
        ParticlePtr p = ParticleManager::getInstance()->getParticle ( i );
        drawParticle ( p );
//         glColor3f ( 1,0,0 );
//         draw ( p->getAABB() );
    }

//     glColor3f ( 1,1,0 );
//     draw ( _grid );

    glPushMatrix();
//     glScalef ( 4,1,4 );
    glColor3f ( 1,0,0 );

    glColor3f ( 1,1,1 );

    glBegin ( GL_QUADS );
    glVertex3f ( -8, 0, -8 );
    glVertex3f ( -8, 0, 8 );
    glVertex3f ( 8, 0, 8 );
    glVertex3f ( 8, 0, -8 );
    glEnd();

    drawLines();

    glPopMatrix();
    glPopMatrix();
    glutSwapBuffers();
}



void mouse ( int button, int state, int x, int y )
{
//     if ( button == GLUT_LEFT_BUTTON && state == GLUT_DOWN )
//     {
//         mouseX = x;
//         mouseY = y;
//     }

    if ( state == GLUT_DOWN )
    {
        cout << "teste " << endl;
        cout << "Button: " << button << endl;
        cout << "x: " << x << " - " << "y: " << y << endl;
    }

    if ( state == GLUT_UP )
    {
        cout << "teste " << endl;
        cout << "Button: " << button << endl;
        cout << "x: " << x << " - " << "y: " << y << endl;
    }
}





void keyboard ( unsigned char key, int x, int y )
{
    ParticlePtr _p  = ParticleManager::getInstance()->getParticle ( 0 );

    if ( key == 'w' )
        _p->addForce ( Vec3 ( 0, 0,-50 ) );
    else if ( key == 's' )
        _p->addForce ( Vec3 ( 0, 0, 50 ) );
    else if ( key == 'a' )
        _p->addForce ( Vec3 ( -50, 0, 0 ) );
    else if ( key == 'd' )
        _p->addForce ( Vec3 ( 50, 0, 0 ) );
    else if ( key == 'r' )
        _cameraPosition += ( Vec3 ( 0, 0.5, 0 ) );
    else if ( key == 'f' )
        _cameraPosition += ( Vec3 ( 0,-0.5, 0 ) );
    else if ( key == 't' )
        _cameraPosition += ( Vec3 ( 0.5, 0, 0 ) );
    else if ( key == 'g' )
        _cameraPosition += ( Vec3 ( -0.5, 0, 0 ) );

//     else if ( key = 'z' )
//     {
//         ParticlePtr p = ParticleManager::getInstance()->newParticle ( 0.5, _cameraPosition, 1.0 );
//         cout << "aaaa " << p->getPosition() << endl;
//     }
//     _grid.getIndexOfPoint ( _cameraPosition, cell );
//     cout << _cameraPosition << endl;
}





void init ( int w, int h )
{
    glClearColor ( 0.0f, 0.0f, 0.0f, 1.0f );
    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    glViewport ( 0, 0, 500, 500 );
    gluPerspective ( 60, 1.0f, 1.0f, 5000.0f );
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();
    glPolygonMode ( GL_FRONT_AND_BACK, GL_LINE );
}





void close()
{
    ParticleManager::getInstance()->destroy();
}





void mouseMove ( int x, int y )
{
//     int xDiff = x-mouseX;
//     int yDiff = y-mouseY;
//
//     mouseX = x;
//     mouseY = y;
//
//     pitch += -yDiff * 1.5f;
//     yaw   += -xDiff * 1.5f;
//     std::cout << x << " " << y << std::endl;
//     pitch = y;
//     yaw = x;

    mouseX = x;
    mouseY = y;

    cout << mouseX << " - " << mouseY << endl;
}

void createParticles()
{
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( -6.0, 0.0,  0.0 ),  0.2f );

    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 6.0, 0.0,  0.0 ),  0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 6.0, 0.0,  0.2 ),  0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 6.0, 0.0,  0.4 ),  0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 6.0, 0.0, -0.2 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 6.0, 0.0, -0.4 ), 0.2f );

    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.6, 0.0,  0.1 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.6, 0.0,  0.3 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.6, 0.0, -0.1 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.6, 0.0, -0.3 ), 0.2f );

    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.2, 0.0,  0.2 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.2, 0.0,  0.0 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 5.2, 0.0, -0.2 ), 0.2f );

    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 4.8, 0.0,  0.1 ), 0.2f );
    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 4.8, 0.0, -0.1 ), 0.2f );

    ParticleManager::getInstance()->newParticle ( 0.3f, Vec3 ( 4.4, 0.0, 0 ), 0.2f );
}



int main ( int argc, char**argv )
{

    glutInit ( &argc,argv );
    glutInitDisplayMode ( GLUT_RGB | GLUT_DOUBLE );
    glutInitWindowSize ( 500, 500 );
    glutCreateWindow ( "Particle Simulator" );

    glutReshapeFunc ( init );

    glutDisplayFunc ( draw );
    glutKeyboardFunc ( keyboard );
    glutMouseFunc ( mouse );
    glutIdleFunc ( update );
    glutCloseFunc ( close );
    glutMotionFunc ( mouseMove );

    createParticles();
    glutMainLoop();
    close();
    return 0;
}























