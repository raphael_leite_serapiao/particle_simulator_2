#include "ParticleManager.h"


ParticleManager* ParticleManager::_instance = 0;




ParticleManager::ParticleManager()
{
    Plane *p1 = new Plane ( Vec3 ( 0, 1,  0 ),   0 );
    Plane *p2 = new Plane ( Vec3 ( 1, 0,  0 ),  -8 );
    Plane *p3 = new Plane ( Vec3 ( -1, 0,  0 ), -8 );
    Plane *p4 = new Plane ( Vec3 ( 0, 0,  1 ), -8 );
    Plane *p5 = new Plane ( Vec3 ( 0, 0, -1 ), -8 );

    _plane.push_back ( p1 );
    _plane.push_back ( p2 );
    _plane.push_back ( p3 );
    _plane.push_back ( p4 );
    _plane.push_back ( p5 );
}




 
ParticleManager::~ParticleManager()
{
    this->removeAll();
    for ( auto iterator1 = _plane.begin(); iterator1 != _plane.end(); iterator1++ )
        delete *iterator1;
}





void ParticleManager::removeAll()
{
    _particles.clear();
}





// void ParticleManager::removeParticle ( const Particle& particle )
// {
//     for(auto it = _particles.begin(); it != _particles.end(); ++it) {
//       if((*it) == &particle)
// 	delete *it;
//     }
// }





void ParticleManager::destroy()
{
    delete _instance;
    _instance = 0;
}





ParticleManager* ParticleManager::getInstance()
{
    if ( _instance == 0 )
        _instance = new ParticleManager();
    return _instance;
}





void ParticleManager::update ( float frameTime, float timeStep, int maxSteps )
{
    static float timeAccumulator = 0.0f;
    timeAccumulator += frameTime;
    int numberOfSteps = 0;

    if ( timeAccumulator >= timeStep )
    {
        numberOfSteps = int ( timeAccumulator / timeStep );
        timeAccumulator -= numberOfSteps * timeStep;
    }

    if ( numberOfSteps )
    {
        const int number = ( numberOfSteps > maxSteps ) ? maxSteps : numberOfSteps;
        for ( unsigned int i = 0; i < number; i++ )
            update ( timeStep );
    }
}





void ParticleManager::update ( const float timeStep )
{
    for ( auto it = _particles.begin(); it != _particles.end(); ++it )
        ( * ( *it ) ).integrate ( timeStep );
    collisionDetection();
}





void ParticleManager::collisionDetection()
{

    for ( auto iterator1 = _plane.begin(); iterator1 != _plane.end(); iterator1++ )
    {

        for ( auto it = _particles.begin(); it != _particles.end(); ++it )
        {
            const float penetration = ( *it )->_radius - ( *iterator1 )->distance ( ( *it )->getPosition() );
            if ( penetration > 0.0f )
            {
                Vec3 planeNormal = ( *iterator1 )->getNormal();
                ( *it )->_position +=  planeNormal * penetration;
                const float nDotV = planeNormal.dot ( ( *it )->_velocity );
                ( *it )->_velocity += planeNormal * ( ( 1.0f + ( *it )->_restitution ) * -nDotV );
            }
        }


    }

    for ( unsigned i = 0u; i < _particles.size(); ++i )
        for ( unsigned j = i+1u; j < _particles.size(); ++j )
            collisionDetection ( ( _particles[i] ), ( _particles[j] ) );
}





void ParticleManager::collisionDetection ( const ParticlePtr& pi, const ParticlePtr& pj ) const
{
    const Vec3 pjToPi = pi->getPosition() - pj->getPosition(); // vector from pj to pi
    const float distance = pjToPi.lenght();
    const float penetration = pj->getRadius() + pi->getRadius() - distance;

    if ( penetration <= 0.0f )
    {
        return;
    }

    Vec3 collisionNormal;

    if ( distance == 0.0f )
    {
        collisionNormal = Vec3 ( 0,1,0 );
    }
    else
    {
        collisionNormal = pjToPi;
        collisionNormal.normalize ( distance );
    }

    const Vec3 relativeVelocity = pi->_velocity - pj->_velocity;
    const float restitution = ( pi->_restitution + pj->_restitution ) / 2.0f;

    const Vec3 impulse = collisionNormal * ( relativeVelocity.dot ( collisionNormal ) * ( ( pi->_mass * pj->_mass ) / ( pi->_mass + pj->_mass ) ) ) * ( 1.0f + restitution );

    // resolve velocities:
    pi->_velocity -= impulse / pi->_mass;
    pj->_velocity += impulse / pj->_mass;

    // resolve positions:
    pi->_position += collisionNormal * penetration/2.0f;
    pj->_position -= collisionNormal * penetration/2.0f;
}
























