#ifndef VEC3_H
#define VEC3_H

#include <cmath>
#include <cassert>
#include <ostream>

class Vec3
{

public:
    Vec3 ( float x = 0, float y = 0, float z = 0 ) : _x ( x ), _y ( y ), _z ( z )
    {
    }

    Vec3 operator+ ( const Vec3& other ) const
    {
        return Vec3 ( _x + other._x, _y + other._y, _z + other._z );
    }

    void operator+= ( const Vec3& other )
    {
        _x += other._x;
        _y += other._y;
        _z += other._z;
    }

    Vec3 operator- ( const Vec3& other ) const
    {
        return Vec3 ( _x -other._x, _y - other._y, _z - other._z );
    }

    void operator -= ( const Vec3& other )
    {
        _x -= other._x;
        _y -= other._y;
        _z -= other._z;
    }

    Vec3 operator* ( const Vec3& other ) const
    {
        return Vec3 ( _x * other._x, _y * other._y, _z * other._z );
    }

    Vec3 operator* ( const float value ) const
    {
        return Vec3 ( _x * value, _y * value, _z * value );
    }

    void operator*= ( const float value )
    {
        _x *= value;
        _y *= value;
        _z *= value;
    }

    Vec3 operator/ ( const float value ) const
    {
        return Vec3 ( _x / value, _y / value, _z / value );
    }

    void operator/= ( const float value )
    {
        _x /= value;
        _y /= value;
        _z /= value;
    }

    float dot ( const Vec3& other ) const
    {
        return ( _x * other._x ) + ( _y * other._y ) + ( _z * other._z );
    }

    Vec3 cross ( const Vec3& other ) const
    {
        return Vec3 ( ( _y * other._z ) - ( other._y * _z ) ,
                      ( _z * other._x ) - ( other._z * _x ),
                      ( _x * other._y ) - ( other._x * _y ) );
    }

    float lenght() const
    {
        return std::sqrt ( lenght2() );
    }

    float lenght2() const
    {
        return dot ( *this );
    }

    float distance ( const Vec3& other ) const
    {
        return std::sqrt ( distance2 ( other ) );
    }

    float distance2 ( const Vec3& other ) const
    {
        return ( other - ( *this ) ).lenght2();
    }

    void normalize()
    {
        normalize ( lenght() );
    }

    void normalize ( const float thisLenght )
    {
        assert ( thisLenght > 0.0f );
        _x /= thisLenght;
        _y /= thisLenght;
        _z /= thisLenght;
    }

    float operator[] ( unsigned i ) const
    {
        assert ( i < 3u );
        return _as_array[i];
    }

    float& operator[] ( unsigned i )
    {
        assert ( i < 3u );
        return _as_array[i];
    }

public:
    union
    {
        struct
        {
            float _x;
            float _y;
            float _z;
        };
        float _as_array[3];
    };

};

std::ostream& operator << ( std::ostream& out, const Vec3& v );

#endif
