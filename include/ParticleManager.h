#ifndef PARTICLEMANAGER_H
#define PARTICLEMANAGER_H

#include "Particle.h"
#include "Plane.h"

#include <vector>
#include <iostream>


using namespace std;

class ParticleManager
{

public:
    static ParticleManager* getInstance();
    static void destroy();

    void update ( float frameTime, float timeStep, int maxSteps );
    void update ( const float timeStep );


    void removeAll();
    //     void removeParticle(const Particle& particle);

public:
    unsigned getNumberParticles() const
    {
        return _particles.size();
    }

    ParticlePtr getParticle ( unsigned particleIndex ) const
    {
        return _particles[particleIndex];
    }

    ParticlePtr newParticle ( float mass, const Vec3& position, float radius )
    {
        ParticlePtr newParticle = std::make_shared<Particle> ( mass,position,radius );
	_particles.push_back(newParticle);
	return newParticle;
    }

private:
    void collisionDetection();
    void collisionDetection ( const ParticlePtr& pi, const ParticlePtr& pj ) const;

private:
    ParticleManager();
    ~ParticleManager();

private:
    static ParticleManager* _instance;
    vector <ParticlePtr> _particles;
    vector<Plane*> _plane;

};

#endif
