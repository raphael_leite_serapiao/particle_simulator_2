

#ifndef GRID_H
#define GRID_H

#include "Aabb.h"

#include <set>
#include <list>
#include <vector>



class Grid
{

public:

    /**
       * The index of a grid cell.
       */
    struct CellIndex
    {
        CellIndex ( unsigned i = 0u, unsigned j = 0u, unsigned k = 0u )
            : i ( i ), j ( j ), k ( k )
        { }

        unsigned operator[] ( unsigned i ) const
        {
            assert ( i < 3u );
            return as_array[i];
        }

        unsigned& operator[] ( unsigned i )
        {
            assert ( i < 3u );
            return as_array[i];
        }

        union
        {
            struct
            {
                unsigned i, j, k;
            };
            unsigned as_array[3];
        };
    };



    /**
     * The data that can be inserted in this grid.
     */
    struct Data
    {
        Data() : userData ( NULL ) { }
        Aabb aabb; // the AABB enclosing this data
        std::list<CellIndex> cells; // the indices of cells that contain this data
        void* userData;
    };




public:
    Grid ( const Aabb& gridAABB, unsigned xNumberOfCells, unsigned yNumberOfCells, unsigned zNumberOfCells );

    Grid ( const Vec3& gridCenter, const Vec3& cellDimensions, unsigned xNumberOfCells, unsigned yNumberOfCells, unsigned zNumberOfCells );

    unsigned getNumberOfCells ( unsigned dimension ) const
    {
        assert ( dimension < 3u );
        return _numberOfCells[dimension];
    }

    void update ( Data* data );

    /**
     * @return TRUE if @param point is inside this grid.
     * If TRUE is returned, the cell index is returned in @param index.
     */
    bool getIndexOfPoint ( const Vec3& point, CellIndex& index ) const;

    const Aabb& getGridAABB() const
    {
        return _gridAABB;
    }

    Aabb getCellAABB ( const CellIndex& index ) const;

    const std::set<Data*>& getCellData ( const CellIndex& index ) const
    {
        return _cells[index.i][index.j][index.k];
    }

    std::set<Data*> getCloseData ( const Data* data ) const;



private:
    void _readCells ( Data* data ) const;

private:
    Aabb _gridAABB;
    unsigned _numberOfCells[3];
    float _cellDimensions[3];
    typedef std::set<Data*> Cell;
    std::vector<std::vector<std::vector<Cell> > > _cells;
};



#endif // GRID_H


























