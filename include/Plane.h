#ifndef PLANE_H
#define PLANE_H

#include "Vec3.h"


class Plane
{
public:
    Plane ( const Vec3& normal, float distance );
    virtual ~Plane();
    const Vec3& getNormal() const;
    float distance(const Vec3& point) const;

private:
    Vec3 _n;
    float _d;

};

#endif



