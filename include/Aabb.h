#ifndef AABB_H
#define AABB_H


#include "Vec3.h"

#include <array>




class Aabb
{
public:
    Aabb() {}
    Aabb ( const Vec3& minPoint, const Vec3& maxPoint ) : _minPoint ( minPoint ), _maxPoint ( maxPoint ) {}
    virtual ~Aabb() {}

    const Vec3& getMin() const
    {
        return _minPoint;
    }

    const Vec3& getMax() const
    {
        return _maxPoint;
    }

    float getWidth() const
    {
        return ( _maxPoint._x - _minPoint._x );
    }

    float getHeight() const
    {
        return ( _maxPoint._y - _minPoint._y );
    }

    float getDepth() const
    {
        return ( _maxPoint._z - _minPoint._z );
    }

    Vec3 getDimensions() const
    {
        return Vec3 ( getWidth(), getHeight(), getDepth() ) ;
    }

    Vec3 getCenter() const
    {
        return ( _minPoint + _maxPoint ) / 2.0f; /*return ( _minPoint + _maxPoint ) /= 2;*/
    }

    bool intersects ( const Aabb& other ) const;

    Vec3 getVertex ( unsigned i ) const;

    std::array<Vec3, 8u> getVertices()  const;

    std::array<std::pair<Vec3, Vec3>,12u> getEdges() const;

private:
    Vec3 _minPoint;
    Vec3 _maxPoint;


};


#endif


