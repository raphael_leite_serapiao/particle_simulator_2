#ifndef PARTICLE_H
#define PARTICLE_H

#include "Vec3.h"
#include "Grid.h"

#include <memory>

class Particle
{

public:
    Particle ( float mass, const Vec3& position, float radius, float damping = 0.97f );
    virtual ~Particle();
    void integrate ( float dt );
    void addForce ( const Vec3& force );
    Vec3 getPosition() const;

    float getRadius() const
    {
        return _radius;
    }


    Aabb getAABB() const
    {
        return Aabb ( getMinPoint(), getMaxPoint() );
    }

    Vec3 getMinPoint() const
    {
        return _position - Vec3 ( _radius, _radius, _radius );
    }
    Vec3 getMaxPoint() const
    {
        return _position + Vec3 ( _radius, _radius, _radius );
    }
    
    Grid::Data* getData() { return &_data; }

private:
    float _mass;
    float _damping;
    float _restitution;
    float _radius;

private:
    Vec3 _position;
    Vec3 _velocity;
    Vec3 _forceAcc;
    Vec3 _gravity;

    Grid::Data _data;

    friend class ParticleManager;
};

typedef std::shared_ptr <Particle> ParticlePtr;


#endif
